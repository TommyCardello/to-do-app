var express = require('express');
var router = express.Router();

var moment = require('moment');

function generateUUID(){
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x7|0x8)).toString(16);
    });
    return uuid;
};
/*
  params:
    tasks  - is araay of objects(tasks) with unique id property
    id     - id of taks that function will use to find corresponding object in tasks array with same id property
  return   - index of found taks or -1 if not found
*/
var findTaskById = function (tasks,lookupId) {
  for (var i =0; i < tasks.length; i++){
    if (tasks[i].id == lookupId){
      return i;
    }
  }
  return -1;
}

var tasks=[
    { 
      id:generateUUID(),
      Finished: moment('15:23 25 May 2014'),  
      Task: 'Fin1|', Created: moment('16:20 12 November 2011'), 
      DueDate: moment('12 12 2012') 
    },
    { id:generateUUID(),
      Finished: moment('25 05 2001'),  
      Task: 'Fin2|', Created: moment('16:20 12 June 2012'), DueDate: moment('12 10 2013')},
    { id:generateUUID(), Finished: null,        Task: 'Com3', Created: moment('16:20 12 December 2013'), DueDate: moment('12 April 2016') },
    { id:generateUUID(), Finished: null,        Task: 'Com4', Created: moment('16:20 12 October 2014'), DueDate: moment('12 May 2015')},
    { id:generateUUID(), Finished: null,        Task: 'overdue', Created: moment('16:20 13 January 2014'), DueDate: moment('10 September 2014')},
  ];
var tasksDeleted=[];
var tasksUnFinished = [];
var fin = [];



router.get('/', function(req, res) {
  res.render('index', { moment:moment, title: 'TaskManager', page:'All', tasks:tasks});
  console.log(tasks);
});

router.get('/finished', function(req, res) {
  var fin = [];
  for (var i = 0; i < tasks.length; i++) {
    var task = tasks[i];
    if ((task.Finished !== 'Invalid date') && (task.Finished !== '')) {
      fin.push(task)
    }
   }
   console.log(fin);
  res.render('finished', {title: 'TaskManager', page: 'finished', tasks:fin});
});

router.get('/unfinished', function(req, res) {
  var tasksUnFinished=[];
  for (var i =0; i < tasks.length; i++){
    var task = tasks[i];   
    task.index=i;
    if ((task.Finished == '') || (task.Finished == 'Invalid date')) {
      tasksUnFinished.push(task)
        var date = moment();
        var taskDueDate = task.DueDate;
        var diff = Math.floor(date.diff(taskDueDate)/(1000*60*60*24));
      if(diff > 0){
        task.class="overdue";
      }
    }
    console.log(tasksUnFinished);
  }
  res.render('unfinished', {title: 'TaskManager', page: 'unfinished', tasks:tasksUnFinished});
});



router.get('/deleted', function(req, res) {
  res.render('deleted', {title: 'TaskManager', page: 'deleted', tasks:tasksDeleted});
});

router.get('/coming', function(req, res) {
  var tasksComing=[];
  var date =  moment();
  for (var i =0; i < tasks.length; i++){
    var task = tasks[i];  
    var nextDay = task.DueDate;
    var diff = Math.floor(date.diff(nextDay)/(1000*60*60*24));
      if (diff < 0){
      tasksComing.push(task)
    }
    console.log(diff);
  }
  
  console.log(diff)
  res.render('coming', {title: 'TaskManager', page: 'coming', tasks:tasksComing});
});

router.get('/task/:id?', function(req, res) {
  console.log(tasks[req.params.index])
  //var task=req.params.index ? tasks[req.params.id] : { id:generateUUID(), Finished: '', Task: '', Created: moment(), DueDate: moment().format('DD MMMM YYYY')} ;
    var index = findTaskById(tasks, req.params.id);

    var task;

    if (index != -1){
      task = tasks[index]
    }
    else { 
      task = {id:generateUUID(), Finished: null, Task: '', Created: moment(), DueDate: moment()}
    }

  res.render('task', { 
    task:task,
    index:index
  });
});

router.get('/delete/:id', function(req, res){
  var index=findTaskById(tasks,req.params.id);
  if(index != -1)
    tasksDeleted.push(tasks.splice(index,1)[0]);
  console.log(req.headers.referer)
  res.redirect(req.headers.referer);
});

router.get('/undelete/:id', function(req, res){
  var index=findTaskById(tasksDeleted,req.params.id);
  if(index != -1)
    tasks.push(tasksDeleted.splice(index, 1)[0]);
  console.log(tasks);
  res.redirect(req.headers.referer);
});

router.get('/finish/:id', function(req, res){
  var index=findTaskById(tasks,req.params.id);
  if(index != -1){
    var taskUnfin = tasks[index];
    taskUnfin.Finished = moment();
  }
  res.redirect(req.headers.referer);
  console.log(req.headers.referer);
}); 

router.post('/task/:id?', function(req, res) {
  var index=findTaskById(tasks,req.params.id);
  
  
  console.log(req.body)
  switch(req.body.action){
    case 'Delete':
      if(index == -1){
         res.redirect('/');
        return;
      }
      tasksDeleted.push(tasks.splice(index,1)[0]);
      break;
    case 'Save':
      var ntask={};
      ntask.Task=req.body.Task;
      ntask.Finished=moment(req.body.Finished);
      ntask.Created=moment();
      ntask.DueDate=moment(req.body.DueDate);
      if(index != -1){
        tasks[index]=ntask;
      }
      else{
        tasks.push(ntask);
      }
      break;
    case 'Cancel':
      break;
      console.log(ntask);
  }
  res.redirect('/');
});

module.exports = router;
